package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    private HashMap<String, Double> bookPrices;

    public Quoter(){

        bookPrices = new HashMap<>();

        bookPrices.put("1", 10.0);
        bookPrices.put("2", 20.0);
        bookPrices.put("3", 30.0);
        bookPrices.put("4", 40.0);
        bookPrices.put("5", 50.0);


    }


    public double getBookPrice(String isbn) {

        return bookPrices.getOrDefault(isbn, 0.0);
    }
}


